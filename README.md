# README #


### What is this repository for? ###

* This Spring Boot application will consume a .csv file, validates it, separates the valid and invalid rows and saves it in database. You can write the valid and invalid rows in seperate .csv file after uncommenting the respective method in FileController.class
* Version 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* Deployment instructions

### Summary of setup ###

* Java version 8
* MySQL server 5.7

Dependencies

* All the dependencies will be downloaded automatically

Database configuration

* create a database called **forex**
* username: root
* password: kasturi66

Compile with Maven and deploy

* Clone the project
* run 
```
#!html

mvn clean compile
```
 or 
```
#!html

mvn clean package
```
 (all the dependencies will be downloaded automatically)
* 
run with 
```
#!python

java -jar target/forex*.jar
```

Open in Browser

```
#!html

http://localhost:8080
```
(you can upload file from here)

 
```
#!html

http://localhost:8080/forex/search
```
 to search the accumulated deals

How it works?

* After uploading the demo files provided with this application. It will separate all the valid and invalid rows and save it to the respective tables. You can view accumulated deals and other summary by providing the full filename in /forex/search page. It will automatically populate the table with the respective valid or invalid deals.

**Uploaded files are stored in /tmp directory**. You can choose your own directory by modifying the code