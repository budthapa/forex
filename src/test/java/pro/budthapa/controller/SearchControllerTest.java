package pro.budthapa.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import pro.budthapa.domain.ValidDeal;
import pro.budthapa.service.AccumulativeDealCountService;
import pro.budthapa.service.InvalidDealService;
import pro.budthapa.service.ValidDealService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SearchController.class)

public class SearchControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@MockBean
	private SessionFactory sessionFactory;

	@MockBean
	private ValidDealService validDealService;

	@MockBean
	private InvalidDealService invalidDealService;

	@MockBean
	private AccumulativeDealCountService accumulativeDealCountService;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build(); // we can
																	// remove
																	// this line
																	// if
																	// @Autowired
																	// is set in
																	// MockMvc
	}

	ValidDeal validDeal;
	List<ValidDeal> dealList;

	@Before
	public void setUpValidDeal() throws Exception {
		validDeal = new ValidDeal();
		dealList = new ArrayList<>();
		validDeal.setId(UUID.randomUUID().toString());
		validDeal.setDate(new Date());
		validDeal.setTimeStamp(new Date().toLocaleString());
		validDeal.setFileName("EURUSD.csv");
		validDeal.setFromCurrencyISOCode("EUR");
		validDeal.setToCurrencyISOCode("USD");
		validDeal.setFromCurrencyISOCodeAmount("3.65");
		validDeal.setToCurrencyISOCodeAmount("3.70");
		validDeal.setAmount("0.05");
		dealList.add(validDeal);
	}

	@Test
	public void showSearchPage() throws Exception {

		assertThat(this.validDealService, is(notNullValue()));

		this.mockMvc.perform(get("/forex/search")).andExpect(status().isOk()).andExpect(view().name("searchResult"))
				.andDo(print());
	}

	@Test
	public void findAllValidDealAndVerifyTheSizeTest() throws Exception {
		List<ValidDeal> deal = validDealService.findAll();
		assertThat(sizeCount(deal.size()), is(notNullValue()));

	}

	@Test
	public void findValidDeal() throws Exception {
		assertEquals(1, dealList.size());
		assertEquals("EURUSD.csv", dealList.get(0).getFileName());
		assertEquals("EUR", dealList.get(0).getFromCurrencyISOCode());
		assertEquals("USD", dealList.get(0).getToCurrencyISOCode());
		assertThat(dealList.get(0).getDate(), is(notNullValue()));
		assertThat(dealList.get(0).getTimeStamp(), is(notNullValue()));
		assertThat(dealList.get(0).getAmount(), notNullValue());
		assertThat(dealList.get(0).getToCurrencyISOCodeAmount(), is(notNullValue()));
		assertThat(dealList.get(0).getFromCurrencyISOCodeAmount(), is(notNullValue()));

	}


	private boolean sizeCount(int size) {
		return size > 0 ? true : false;
	}


}
