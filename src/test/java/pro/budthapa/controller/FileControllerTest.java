package pro.budthapa.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import pro.budthapa.service.AccumulativeDealCountService;
import pro.budthapa.service.FileService;
import pro.budthapa.service.InvalidDealService;
import pro.budthapa.service.ValidDealService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FileController.class)
public class FileControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private SessionFactory sessionFactory;

	@MockBean
	private ValidDealService validDealService;
	
	@MockBean
	private FileService fileService;

	@MockBean
	private InvalidDealService invalidDealService;

	@MockBean
	private AccumulativeDealCountService accumulativeDealCountService;

	@Test
	public void testIndexPage() throws Exception{
		this.mockMvc.perform(get("/").contentType(MediaType.TEXT_PLAIN)).andExpect(status().isOk()).andDo(print());
	}
	
	@Test
	public void saveUploadedFile() throws Exception{
		/*
		File file = new File("/home/budthapa/Documents/Projects/EURUSD.csv");
		FileInputStream fis = new FileInputStream(file);
		
		byte fileContent[] = new byte[(int) file.length()];
		
		fis.read(fileContent);
		
		MockMultipartFile multipartFile = new MockMultipartFile("/home/budthapa/Documents/Projects/EURUSD.csv", fis);
//		MockMultipartFile multipartFile = new MockMultipartFile("file", "EURUSD.csv", "text/plain", "EURUSD.csv".getBytes());
		
		
		mockMvc.perform(fileUpload("/forex/upload").file(multipartFile).param("fileName", "EURUSD.csv"))
//		.andExpect(status().isOk())
		.andExpect(model().attribute("message", "filed upload success"))
		
		.andDo(print());
		
		then(this.fileService).should().saveToTempLocation(multipartFile);
		
		 */
	}
	
}
