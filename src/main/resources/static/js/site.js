	$(document).ready(function() {
		$("#fileUpload").validate({
			rules: {
				fileName: {
	                required: true,
	                extension: "csv",
	                filesize: 5242880
	            }
	        },
	        messages: { 
	        	fileName: "File must be CSV, less than 5MB" 
	        }
		});

		$.validator.addMethod('filesize', function(value, element, param) {
		    // param = size (in bytes)
		    // element = element to validate (<input>)
		    // value = value of the element (file name)
		    return this.optional(element) || (element.files[0].size <= param) 
		});

	});
