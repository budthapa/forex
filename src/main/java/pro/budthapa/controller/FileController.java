package pro.budthapa.controller;

import java.io.FileNotFoundException;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import pro.budthapa.domain.AccumulativeDealCount;
import pro.budthapa.domain.InvalidDeal;
import pro.budthapa.domain.ValidDeal;
import pro.budthapa.helper.FileEnum;
import pro.budthapa.helper.FileHelper;
import pro.budthapa.service.AccumulativeDealCountService;
import pro.budthapa.service.FileService;
import pro.budthapa.service.InvalidDealService;
import pro.budthapa.service.ValidDealService;

@Controller
public class FileController {
	Logger log = Logger.getLogger(FileController.class);

	private static String INDEX = "index";

	private final int FILE_SIZE = 1024 * 1024 * 10; // 10MB

	@Autowired
	private FileService fileService;

	@Autowired
	private ValidDealService validDealService;

	@Autowired
	private InvalidDealService invalidDealService;

	@Autowired
	private AccumulativeDealCountService accumulativeDealCountService;

	@GetMapping({ "/", "/forex/upload" })
	public String index() {
		return INDEX;
	}

	@PostMapping("/forex/upload")
	public String uploadFile(@RequestParam("fileName") MultipartFile file, Model model) throws FileNotFoundException {

		boolean checkFileType = new FileHelper().checkValidCSVFileType(file);

		if (!checkFileType) {
			log.warn("Not a valid csv file");
			model.addAttribute("invalidFileType", true);
			return INDEX;
		}

		if (file.getSize() > FILE_SIZE) {
			model.addAttribute("invalidFileSize", true);
			return INDEX;
		}

		if (!file.isEmpty()) {
			boolean fileExists = fileService.saveToTempLocation(file);
			if (fileExists) {
				log.info(file.getOriginalFilename() + " already exists in system");
				model.addAttribute("fileExists", fileExists);
				return INDEX;
			}
		}

		Reader in = fileService.selectTempFile(file.getOriginalFilename());
		boolean validFile = parseCSVFile(in, file.getOriginalFilename(), model);
		if (validFile) {
			model.addAttribute("fileUploadSuccessful", true);
		}
		return INDEX;
	}

	private boolean parseCSVFile(Reader in, String fileName, Model model) {

		try {
			List<CSVRecord> validList = new ArrayList<>();
			List<CSVRecord> invalidList = new ArrayList<>();

			Iterable<CSVRecord> record = CSVFormat.EXCEL.withFirstRecordAsHeader().withAllowMissingColumnNames()
					.withIgnoreHeaderCase().parse(in);
			
			FileHelper fileHelper = new FileHelper();
			extractColumnHeader(fileHelper, record.iterator().next().toString());
			
			if (fileHelper.getDateHeader()!=null && fileHelper.getTimeStampHeader() != null && fileHelper.getFromCurrencyHeader() != null &&
					fileHelper.getToCurrencyHeader() != null && fileHelper.getSpreadHeader() != null) {

				System.out.println("date " + fileHelper.getDateHeader() + " timestamp " + fileHelper.getTimeStampHeader() + " from curr " + 
				fileHelper.getFromCurrencyHeader() + "to cu"+ fileHelper.getToCurrencyHeader() + " spread " + fileHelper.getSpreadHeader());

				for (CSVRecord r : record) {

					if (/* r.get(ID).isEmpty() || */r.get(fileHelper.getDateHeader()).isEmpty() || r.get(fileHelper.getTimeStampHeader()).isEmpty()
							|| r.get(fileHelper.getFromCurrencyHeader()).isEmpty() || r.get(fileHelper.getToCurrencyHeader()).isEmpty()
							|| r.get(fileHelper.getSpreadHeader()).isEmpty()) {
						invalidList.add(r);
					} else {
						validList.add(r);
					}

				}
			} else {
				model.addAttribute("invalidFileType", true);
				return false;
			}

			log.info("Now writing in database started at :" + new Date().toString());
			saveInvalidDealToDb(invalidList, fileName, fileHelper);
			saveValidDealToDb(validList, fileName, fileHelper);
			log.info("Writing in database completed at :" + new Date().toString());

			accumulateCountOfValidDeal(validList.size(), fileHelper.getFromCurrencyHeader());
			/*
			 * log.info("Exporting as csv started at :" + new
			 * Date().toString());
			 * 
			 * FileHelper fh = new FileHelper(); fh.validCSVWriter(validList,
			 * fileName); fh.invalidCSVWriter(invalidList, fileName);
			 * 
			 * log.info("Exporting as csv completed at :" + new
			 * Date().toString());
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/*
	 * Accumulate count of deals that are valid and in ordering currency
	 */

	private void accumulateCountOfValidDeal(int validRecordsCount, String currency) {
		AccumulativeDealCount accumulativeDealCount = accumulativeDealCountService.findByFromCurrencyIsoCode(currency);
		if (accumulativeDealCount != null) {
			int latestCount = accumulativeDealCount.getCountOfDeal() + validRecordsCount;
			accumulativeDealCount.setCountOfDeal(latestCount);
			accumulativeDealCount.setFromCurrencyIsoCode(currency);
			accumulativeDealCountService.updateDealCount(accumulativeDealCount);
		} else {
			accumulativeDealCount = new AccumulativeDealCount();
			accumulativeDealCount.setCountOfDeal(validRecordsCount);
			accumulativeDealCount.setFromCurrencyIsoCode(currency);
			accumulativeDealCountService.saveDealCount(accumulativeDealCount);
		}

	}

	private FileHelper extractColumnHeader(FileHelper fileHelper, String str) {

		String s = str.substring(str.indexOf("={") + 2, str.indexOf("},"));
		String[] arr = s.split(",");

		for (int i = 0; i < arr.length; i++) {
			/*
			 * System.out.println(arr[i].trim());
			 * if(arr[i].trim().endsWith("=0")){ ID=arr[i].substring(0,
			 * arr[i].indexOf("=0")).trim(); }else
			 */
			if (arr[i].trim().endsWith("=0")) {
				fileHelper.setDateHeader(arr[i].substring(0, arr[i].indexOf("=0")).trim());
			} else if (arr[i].trim().endsWith("=1")) {
				fileHelper.setTimeStampHeader(arr[i].substring(0, arr[i].indexOf("=1")).trim());
			} else if (arr[i].trim().endsWith("=2")) {
				fileHelper.setFromCurrencyHeader(arr[i].substring(0, arr[i].indexOf("=2")).trim());
			} else if (arr[i].trim().endsWith("=3")) {
				fileHelper.setToCurrencyHeader(arr[i].substring(0, arr[i].indexOf("=3")).trim());
			} else if (arr[i].trim().endsWith("=4")) {
				fileHelper.setSpreadHeader(arr[i].substring(0, arr[i].indexOf("=4")).trim());
			}
		}
		return fileHelper;
	}

	private void saveInvalidDealToDb(List<CSVRecord> invalidList, String fileName, FileHelper fileHelper) {
		List<InvalidDeal> list = new ArrayList<>();
		InvalidDeal deal = null;
		for (CSVRecord r : invalidList) {
			deal = new InvalidDeal();
			try {
				if (!r.get("Date").isEmpty()) {
					deal.setDate(new SimpleDateFormat(FileEnum.DATE_PATTERN).parse(r.get(fileHelper.getDateHeader())));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			deal.setId(UUID.randomUUID().toString());
			deal.setTimeStamp(r.get(fileHelper.getTimeStampHeader()));
			deal.setFromCurrencyISOCode(fileHelper.getFromCurrencyHeader());
			deal.setToCurrencyISOCode(fileHelper.getToCurrencyHeader());
			deal.setFromCurrencyISOCodeAmount(r.get(fileHelper.getFromCurrencyHeader()));
			deal.setToCurrencyISOCodeAmount(r.get(fileHelper.getToCurrencyHeader()));
			deal.setAmount(r.get(fileHelper.getSpreadHeader()));
			deal.setFileName(fileName);
			// deal.setFileName(fileName + FileEnum.INVALID_STR + new Date() +
			// FileEnum.CSV_SUFFIX);
			list.add(deal);
		}
		invalidDealService.saveInvalidDeal(list);

	}

	private void saveValidDealToDb(List<CSVRecord> validList, String fileName, FileHelper fileHelper) {
		List<ValidDeal> list = new ArrayList<>();
		ValidDeal deal = null;
		for (CSVRecord r : validList) {
			deal = new ValidDeal();
			try {
				deal.setDate(new SimpleDateFormat(FileEnum.DATE_PATTERN).parse(r.get(fileHelper.getDateHeader())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			deal.setId(UUID.randomUUID().toString());
			deal.setTimeStamp(r.get(fileHelper.getTimeStampHeader()));
			deal.setFromCurrencyISOCode(fileHelper.getFromCurrencyHeader());
			deal.setToCurrencyISOCode(fileHelper.getToCurrencyHeader());
			deal.setFromCurrencyISOCodeAmount(r.get(fileHelper.getFromCurrencyHeader()));
			deal.setToCurrencyISOCodeAmount(r.get(fileHelper.getToCurrencyHeader()));
			deal.setAmount(r.get(fileHelper.getSpreadHeader()));
			deal.setFileName(fileName);
			// deal.setFileName(fileName + FileEnum.VALID_STR + new Date() +
			// FileEnum.CSV_SUFFIX);
			list.add(deal);
		}
		validDealService.saveDeal(list);
	}

}
