package pro.budthapa.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pro.budthapa.domain.AccumulativeDealCount;
import pro.budthapa.domain.InvalidDeal;
import pro.budthapa.domain.ValidDeal;
import pro.budthapa.service.AccumulativeDealCountService;
import pro.budthapa.service.InvalidDealService;
import pro.budthapa.service.ValidDealService;

@Controller
public class SearchController {
	Logger log = LoggerFactory.getLogger(SearchController.class);
	
	private static final String SEARCH_PAGE = "search";

	private static final String SEARCH_RESULT_PAGE = "searchResult";

	@Autowired
	private ValidDealService validDealService;

	@Autowired
	private InvalidDealService invalidDealService;

	@Autowired
	private AccumulativeDealCountService accumulativeDealCountService;

	/*
	 * @GetMapping("/forex/search") public String searchByFileName() { return
	 * SEARCH_RESULT_PAGE; }
	 */
	@GetMapping("/forex/search")
	public String searchByFileName(Model model) {

			List<ValidDeal> validDeal = validDealService.findAll();
			List<InvalidDeal> invalidDeal = invalidDealService.findAll();

			List<AccumulativeDealCount> dealCount = accumulativeDealCountService.findAll();
			
			
			int totalDealCount = validDeal.size() + invalidDeal.size();
			System.out.println("valid deal " + validDeal.size());
			System.out.println("invalid deal " + invalidDeal.size());

			double validDealAmount = 0;
			double validFromAmount = 0.0;
			double validToAmount = 0.0;

			for (ValidDeal deal : validDeal) {
				validDealAmount += Double.parseDouble(deal.getAmount());
				validFromAmount += Double.parseDouble(deal.getFromCurrencyISOCodeAmount());
				validToAmount += Double.parseDouble(deal.getToCurrencyISOCodeAmount());
			}

			model.addAttribute("totalDealCount", totalDealCount);
			model.addAttribute("validDealAmount", validDealAmount);
			model.addAttribute("validFromAmount", validFromAmount);
			model.addAttribute("validToAmount", validToAmount);
			
			
			model.addAttribute("dealCount", dealCount);


		return SEARCH_RESULT_PAGE;
	}

	@ResponseBody
	@PostMapping(value = { "/forex/search/data" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public Object searchByFileNameValid(@RequestParam("filename") String fileName,
			@RequestParam("select-type") String selectType) {
		List<ValidDeal> validDeal = new ArrayList<>();
		List<InvalidDeal> invalidDeal = new ArrayList<>();

		if (fileName.trim() != null && fileName.endsWith(".csv")) {
			if (selectType.equals("validData")) {
				log.info("Fetching the valid data and response as JSON");
				validDeal = validDealService.findByFileName(fileName);
				return validDeal;
			}else{
				log.info("Fetching the invalid data and response as JSON");
				invalidDeal = invalidDealService.findByFileName(fileName);
				return invalidDeal;
			}
		}
		return null;
	}

}
