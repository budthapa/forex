package pro.budthapa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.budthapa.domain.AccumulativeDealCount;

@Repository
public interface AccumulativeDealCountRepository extends JpaRepository<AccumulativeDealCount, Long>{
	AccumulativeDealCount findByFromCurrencyIsoCode(String fromCurrencyIsoCode);
}
