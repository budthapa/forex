package pro.budthapa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.budthapa.domain.InvalidDeal;

@Repository
public interface InvalidDealRepository extends JpaRepository<InvalidDeal, Long>{
	List<InvalidDeal> findByFileName(String fileName);
}
