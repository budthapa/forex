package pro.budthapa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.budthapa.domain.ValidDeal;

@Repository
public interface ValidDealRepository extends JpaRepository<ValidDeal, Long>{

	List<ValidDeal> findByFileName(String filename);

}
