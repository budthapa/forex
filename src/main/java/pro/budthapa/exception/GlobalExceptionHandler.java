package pro.budthapa.exception;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pro.budthapa.controller.FileController;

@ControllerAdvice
public class GlobalExceptionHandler {
	Logger log = Logger.getLogger(FileController.class);
	/**
	 * Throws exception while user tries to upload file larger than specified size.
	 * @param model
	 * @return
	 * @throws IOException 
	 */
	@ExceptionHandler({MultipartException.class, IOException.class})
	public String maxFileSizeError(MultipartException e, RedirectAttributes ra){
		
		//The Model may not be a parameter of any @ExceptionHandler method
		ModelAndView model = new ModelAndView();
		log.warn("Throwing an file size exceed exception");
		log.warn(e.getMessage());
		model.addObject("invalidFileSize", true);
        ra.addFlashAttribute("invalidFileSize", true);
		model.setViewName("index");
		
		return "redirect:/";
	}
}
