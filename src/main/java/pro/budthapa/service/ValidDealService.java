package pro.budthapa.service;

import java.util.List;

import pro.budthapa.domain.ValidDeal;

public interface ValidDealService {
	public ValidDeal saveDeal(ValidDeal validDeal);
	public void saveDeal(List<ValidDeal> validDeal);
	public List<ValidDeal> findByFileName(String filename);
	public List<ValidDeal> findAll();
}
