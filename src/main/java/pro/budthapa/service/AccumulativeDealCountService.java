package pro.budthapa.service;

import java.util.List;

import pro.budthapa.domain.AccumulativeDealCount;

public interface AccumulativeDealCountService {
	AccumulativeDealCount saveDealCount(AccumulativeDealCount deal);
	AccumulativeDealCount updateDealCount(AccumulativeDealCount deal);
	AccumulativeDealCount findByFromCurrencyIsoCode(String fromCurrencyIsoCode);
	List<AccumulativeDealCount> findAll();
}
