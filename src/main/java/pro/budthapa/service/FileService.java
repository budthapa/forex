package pro.budthapa.service;

import java.io.FileNotFoundException;
import java.io.Reader;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
	public boolean saveToTempLocation(MultipartFile multipartFile);
	public Reader selectTempFile(String fileName) throws FileNotFoundException;
}
