package pro.budthapa.service;

import java.util.List;

import pro.budthapa.domain.InvalidDeal;

public interface InvalidDealService {
	public void saveInvalidDeal(List<InvalidDeal> invalidDeal);

	public List<InvalidDeal> findByFileName(String fileName);

	public List<InvalidDeal> findAll();
}
