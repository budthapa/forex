package pro.budthapa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pro.budthapa.domain.InvalidDeal;
import pro.budthapa.repository.InvalidDealRepository;
import pro.budthapa.service.InvalidDealService;

@Service
public class InvalidDealServiceImpl implements InvalidDealService{

	@Autowired
	private InvalidDealRepository invalidDealRepository;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void saveInvalidDeal(List<InvalidDeal> invalidDeal) {
		int batchSize=7000;
		int numberOfElements=invalidDeal.size();

		int size = invalidDeal.size();
		
		Session session = sessionFactory.openSession();
		Transaction tx;// = session.beginTransaction();
		
		int i=0;
		int num = 7000;
		
		while (numberOfElements > 0) {
		  numberOfElements -= num;
		  
		  tx = session.beginTransaction();
		  for (;i <batchSize; i++) {
			  if(i==size){
				  break;
			  }
			  session.save(invalidDeal.get(i));				  
		  }
			
		  session.flush();
		  tx.commit();

		  session.clear();
		  batchSize +=num;
		}
	}

	@Override
	public List<InvalidDeal> findByFileName(String fileName) {
		return invalidDealRepository.findByFileName(fileName);
	}

	@Override
	public List<InvalidDeal> findAll() {
		return invalidDealRepository.findAll();
	}
	
	
}
