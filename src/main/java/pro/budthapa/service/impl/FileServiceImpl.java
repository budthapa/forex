package pro.budthapa.service.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pro.budthapa.helper.FileEnum;
import pro.budthapa.service.FileService;

@Service
public class FileServiceImpl implements FileService {

	@Override
	public boolean saveToTempLocation(MultipartFile file) {
		try {
			Path path = Paths.get(FileEnum.TEMP_LOCATION + file.getOriginalFilename());

			if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
				return true;
			}
			Files.write(path, file.getBytes());
		} catch (IOException e) {
			e.getStackTrace();
		}
		return false;

	}

	@Override
	public Reader selectTempFile(String fileName) throws FileNotFoundException{
		return new FileReader(FileEnum.TEMP_LOCATION + fileName);
	}

}
