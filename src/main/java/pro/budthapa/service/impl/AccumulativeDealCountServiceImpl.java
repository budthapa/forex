package pro.budthapa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pro.budthapa.domain.AccumulativeDealCount;
import pro.budthapa.repository.AccumulativeDealCountRepository;
import pro.budthapa.service.AccumulativeDealCountService;

@Service
public class AccumulativeDealCountServiceImpl implements AccumulativeDealCountService{

	@Autowired
	private AccumulativeDealCountRepository accumulativeDealCountRepository;
	
	@Override
	public AccumulativeDealCount saveDealCount(AccumulativeDealCount deal) {
		return accumulativeDealCountRepository.save(deal);
	}

	@Override
	public AccumulativeDealCount updateDealCount(AccumulativeDealCount deal) {
		return accumulativeDealCountRepository.save(deal);
	}

	@Override
	public AccumulativeDealCount findByFromCurrencyIsoCode(String fromCurrencyIsoCode){
		return accumulativeDealCountRepository.findByFromCurrencyIsoCode(fromCurrencyIsoCode);
	}

	@Override
	public List<AccumulativeDealCount> findAll() {
		return accumulativeDealCountRepository.findAll();
	}
	
}
