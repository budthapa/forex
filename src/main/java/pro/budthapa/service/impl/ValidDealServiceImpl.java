package pro.budthapa.service.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pro.budthapa.domain.ValidDeal;
import pro.budthapa.repository.ValidDealRepository;
import pro.budthapa.service.ValidDealService;

@Service
@Transactional
public class ValidDealServiceImpl implements ValidDealService {

	@Autowired
	private ValidDealRepository validDealRepository;

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ValidDeal saveDeal(ValidDeal validDeal) {
		return validDealRepository.save(validDeal);
	}


	@Override
	public void saveDeal(List<ValidDeal> validDeal) {
		
		int batchSize=7000;
		int numberOfElements=validDeal.size();

		int size = validDeal.size();
		
		Session session = sessionFactory.openSession();
		Transaction tx;// = session.beginTransaction();
		
		int i=0;
		int num = 7000;
		
		while (numberOfElements > 0) {
		  numberOfElements -= num;
		  
		  tx = session.beginTransaction();
		  for (;i <batchSize; i++) {
			  if(i==size){
				  break;
			  }
			  session.save(validDeal.get(i));				  
		  }
			
		  session.flush();
		  tx.commit();

		  session.clear();
		  batchSize +=num;
		}
		
	}


	@Override
	public List<ValidDeal> findByFileName(String filename) {
		return validDealRepository.findByFileName(filename);
	}


	@Override
	public List<ValidDeal> findAll() {
		return validDealRepository.findAll();
	}

}
