package pro.budthapa.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ValidDeal {
	@Id
	private String id;
	
	@Column(name="from_currency_iso_code")
	private String fromCurrencyISOCode;
	
	@Column(name="to_currency_iso_code")
	private String toCurrencyISOCode;
	
	@Column(name="from_currency_iso_code_amount")
	private String fromCurrencyISOCodeAmount;
	
	@Column(name="to_currency_iso_code_amount")
	private String toCurrencyISOCodeAmount;
	
	private Date date;
	private String timeStamp;
	
	private String amount;
	
	private String fileName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFromCurrencyISOCode() {
		return fromCurrencyISOCode;
	}

	public void setFromCurrencyISOCode(String fromCurrencyISOCode) {
		this.fromCurrencyISOCode = fromCurrencyISOCode;
	}

	public String getToCurrencyISOCode() {
		return toCurrencyISOCode;
	}

	public void setToCurrencyISOCode(String toCurrencyISOCode) {
		this.toCurrencyISOCode = toCurrencyISOCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getFromCurrencyISOCodeAmount() {
		return fromCurrencyISOCodeAmount;
	}

	public void setFromCurrencyISOCodeAmount(String fromCurrencyISOCodeAmount) {
		this.fromCurrencyISOCodeAmount = fromCurrencyISOCodeAmount;
	}

	public String getToCurrencyISOCodeAmount() {
		return toCurrencyISOCodeAmount;
	}

	public void setToCurrencyISOCodeAmount(String toCurrencyISOCodeAmount) {
		this.toCurrencyISOCodeAmount = toCurrencyISOCodeAmount;
	}
	
}
