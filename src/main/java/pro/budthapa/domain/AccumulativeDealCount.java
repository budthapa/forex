package pro.budthapa.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="accumulative_deal_count")
public class AccumulativeDealCount {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="ordering_currency")
	private String fromCurrencyIsoCode;
	
	private int countOfDeal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFromCurrencyIsoCode() {
		return fromCurrencyIsoCode;
	}

	public void setFromCurrencyIsoCode(String fromCurrencyIsoCode) {
		this.fromCurrencyIsoCode = fromCurrencyIsoCode;
	}

	public int getCountOfDeal() {
		return countOfDeal;
	}

	public void setCountOfDeal(int countOfDeal) {
		this.countOfDeal = countOfDeal;
	}
	
}
