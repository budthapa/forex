package pro.budthapa.helper;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

public class FileHelper {

	// private static String ID;// = "Id";
	private String dateHeader;// = "Date";
	private String timeStampHeader;// = "TimeStamp";
	private String spreadHeader;// = "Spread";
	private String fromCurrencyHeader;// = "TimeStamp";
	private String toCurrencyHeader;// = "Spread";

	public FileHelper() {
	}

	public String getDateHeader() {
		return dateHeader;
	}

	public void setDateHeader(String dateHeader) {
		this.dateHeader = dateHeader;
	}

	public String getTimeStampHeader() {
		return timeStampHeader;
	}

	public void setTimeStampHeader(String timeStampHeader) {
		this.timeStampHeader = timeStampHeader;
	}

	public String getSpreadHeader() {
		return spreadHeader;
	}

	public void setSpreadHeader(String spreadHeader) {
		this.spreadHeader = spreadHeader;
	}

	public String getFromCurrencyHeader() {
		return fromCurrencyHeader;
	}

	public void setFromCurrencyHeader(String fromCurrencyHeader) {
		this.fromCurrencyHeader = fromCurrencyHeader;
	}

	public String getToCurrencyHeader() {
		return toCurrencyHeader;
	}

	public void setToCurrencyHeader(String toCurrencyHeader) {
		this.toCurrencyHeader = toCurrencyHeader;
	}

	public void invalidCSVWriter(List<CSVRecord> invalidList, String fileName) {
		CSVPrinter printer = null;
		CSVFormat format = CSVFormat.DEFAULT.withHeader(FileEnum.DATE.toString(), FileEnum.TIMESTAMP.toString(),
				FileEnum.FROM_CURRENCY.toString(), FileEnum.TO_CURRENCY.toString(), FileEnum.SPREAD.toString());

		try (Writer invalidWriter = new FileWriter(
				FileEnum.TEMP_LOCATION + fileName + FileEnum.INVALID_STR + new Date() + FileEnum.CSV_SUFFIX)) {
			String recordSeperator = CSVFormat.DEFAULT.getRecordSeparator();
			printer = new CSVPrinter(invalidWriter, format.withRecordSeparator(recordSeperator));
			for (int i = 0; i < invalidList.size(); i++) {
				printer.printRecord(invalidList.get(i));

			}
			printer.close();
		} catch (IOException e) {
			e.getMessage();
		}

	}

	public void validCSVWriter(List<CSVRecord> validList, String fileName) {
		CSVPrinter printer = null;
		CSVFormat format = CSVFormat.DEFAULT.withHeader(FileEnum.DATE.toString(), FileEnum.TIMESTAMP.toString(),
				FileEnum.FROM_CURRENCY.toString(), FileEnum.TO_CURRENCY.toString(), FileEnum.SPREAD.toString());

		try (Writer validWriter = new FileWriter(
				FileEnum.TEMP_LOCATION + fileName + FileEnum.VALID_STR + new Date() + FileEnum.CSV_SUFFIX)) {
			printer = new CSVPrinter(validWriter, format);
			printer.printRecords(validList);
			printer.close();
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public boolean checkValidCSVFileType(MultipartFile file) {
		if (!file.getContentType().trim().endsWith(FileEnum.FILE_FORMAT_CSV)) {
			return false;
		}
		return true;
	}
}
